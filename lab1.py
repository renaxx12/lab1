entrada = input("Ingrese N C K separados por espacio\n")
NCK = entrada.split()

if len(NCK) != 3:
    print("ingreso no valido")
    exit(1)

else:
    N = int(NCK[0])
    C = int(NCK[1])
    K = int(NCK[2])

entradas = []
for n in range(N):
    prueba = input(f"Ingrese {C} numeros para la entrada {n+1}\n")
    entradas.append(prueba)
#en las siguiente lineas se validarán las entradas, y se guardarán aquí
entrada_valida = []

for entrada in entrada:

    aux = entrada.split()

    if len(aux) != C:
        print("la entrada no es válida")
        continue
    
    valida = True 

    for i in aux:
        try:
            n = int(i)
            if n > K:
                valida = False
                break
        except ValueError:
            print("ingreso inválido")
            valida = False
            break

        if valida:
            entrada_valida.append(aux)

print("las entradas válidas son: \n")

for valida in entrada_valida:
    print(valida)

    


